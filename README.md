# vue-music163

> A Vue.js project for music
### Use
* vuejs-2.0
* vue-cli
* vue-router
* vuex
* mint-ui


## Support
* Chrome
* Firefox
* Mobile browser

## Package for app
* Appcan
* Apicloud

## Other link
- vue-music-qq : https://github.com/pluto1114/vue-music-qq

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

## Demo site(aliyun)

# http://119.27.189.70/vue-music163/#/



## Screenshots
![image](https://images.gitee.com/uploads/images/2019/1214/162836_7df064c1_300982.png)

![image](https://images.gitee.com/uploads/images/2019/1214/162836_234b2316_300982.png)

![image](https://images.gitee.com/uploads/images/2019/1214/162838_3663df9d_300982.png)

# Conatct

- Tencent QQ: 342878509
- E-mail: xiongmao1114@163.com
- Git@OSC: http://git.oschina.net/xiongmao1114/vue-music163
- Github: https://github.com/pluto1114/vue-music163

